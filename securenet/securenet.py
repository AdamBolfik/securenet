import requests
import datetime
import json
import re


class Securenet:
    def __init__(self, securenet_id, securenet_key, url="https://gwapi.demo.securenet.com/api/"):
        self.securenet_id = str(securenet_id)
        self.securenet_key = str(securenet_key)
        self.url = url
        self.auth = (self.securenet_id, self.securenet_key)


    ################################################################################
    # JSON Utilities
    ################################################################################

    _camel_pat = re.compile(r'([A-Z])')
    _under_pat = re.compile(r'_([a-z])')


    def _camel_to_underscore(self, name):
        return self._camel_pat.sub(lambda x: '_' + x.group(1).lower(), name)


    def _underscore_to_camel(self, name):
        return self._under_pat.sub(lambda x: x.group(1).upper(), name)


    def _date_hook(self, json_dict):
        for (key, value) in json_dict.items():
            try:
                json_dict[key] = datetime.datetime.strptime(value, "%Y-%m-%dT%H:%M:%SZ")
            except (TypeError, ValueError):
                pass
        return json_dict


    def _convert_json(self, obj, convert):
        new_d = {}
        if isinstance(obj, list):
            new_l = []
            for value in obj:
                if isinstance(value, dict):
                    new_l.append(self._convert_json(value, convert))
                elif isinstance(value, list):
                    new_l.append(self._convert_json(value, convert))
                else:
                    new_l.append(value)
            return new_l
        elif isinstance(obj, dict):
            for key, value in obj.items():
                if isinstance(value, dict):
                    new_d[convert(key)] = self._convert_json(value, convert)
                elif isinstance(value, list):
                    new_d[convert(key)] = self._convert_json(value, convert)
                else:
                    new_d[convert(key)] = value
        return new_d


    def _json_load(self, text):
        data = json.loads(text, object_hook=self._date_hook)
        return self._convert_json(data, self._camel_to_underscore)


    def _json_dump(self, data):
        data = self._convert_json(data, self._underscore_to_camel)
        return json.dumps(data)


    ################################################################################
    # Socket Utilities
    ################################################################################

    _headers = {'content-type': 'application/json'}


    def _error(self, message):
        return {
            'success': False,
            'message': message,
            'result': 'COMMUNICATION_ERROR'
        }


    def _post(self, path, payload=None):
        url = self.url + path
        data = self._json_dump(payload) if payload else {}
        r = requests.post(url, auth=self.auth, data=data, headers=self._headers)
        return self._json_load(r.text)


    def _put(self, path, payload=None):
        url = self.url + path
        data = self._json_dump(payload) if payload else {}
        r = requests.put(url, auth=self.auth, data=data, headers=self._headers)
        return self._json_load(r.text)


    def _delete(self, path, payload=None):
        url = self.url + path
        data = self._json_dump(payload) if payload else {}
        r = requests.delete(url, auth=self.auth, data=data, headers=self._headers)
        return self._json_load(r.text)


    def _get(self, path):
        url = self.url + path
        r = requests.get(url, auth=self.auth, headers=self._headers)
        return self._json_load(r.text)


    ################################################################################
    # Batch Processing
    ################################################################################

    def get_batch(self, batch_id="Current"):
        return self._get("batches/" + str(batch_id))


    def get_current_batch(self):
        return self.get_batch()


    def close_batch(self):
        return self._post("batches/Close")


    ################################################################################
    # Transaction Processing
    ################################################################################

    def authorize(self, req):
        return self._post("Payments/Authorize", req)


    def capture(self, req):
        return self._post("/Payments/Capture", req)


    def charge(self, req):
        return self._post("/Payments/Charge", req)


    def verify(self, req):
        return self._post("/Payments/Verify", req)
        

    ################################################################################
    # Vault
    ################################################################################

    def create_customer(self, req):
        return self._post("/Customers", req)


    def create_customer_and_payment_method(self, req):
        return self._post("/Customers/Payments", req)
        

    def get_customer(self, req):
        if isinstance(req, dict):
            if 'customer_id' in req:
                customer_id = str(req['customer_id'])
            else:
                return self._error("customer_id is required")
        else:
            customer_id = str(req)
        return self._get("/Customers/" + customer_id)


    def update_customer(self, req):
        if 'customer_id' in req:
            customer_id = str(req['customer_id'])
        else:
            return self._error("customer_id is required")
        return self._put("/Customers/" + customer_id, req)


    def create_customer_payment_method(self, req):
        if 'customer_id' in req:
            customer_id = str(req['customer_id'])
        else:
            return self._error("customer_id is required")
        return self._post("/Customers/" + customer_id + "/PaymentMethod", req)


    def get_customer_payment_method(self, req):
        if 'customer_id' in req:
            customer_id = str(req['customer_id'])
        else:
            return self._error("customer_id is required")
        if 'payment_method_id' in req:
            payment_method_id = str(req['payment_method_id'])
        else:
            return self._error("payment_method_id is required")
        return self._get("/Customers/" + customer_id + "/PaymentMethod/" + payment_method_id)


    def delete_customer_payment_method(self, req):
        if 'customer_id' in req:
            customer_id = str(req['customer_id'])
        else:
            return self._error("customer_id is required")
        if 'payment_method_id' in req:
            payment_method_id = str(req['payment_method_id'])
        else:
            return self._error("payment_method_id is required")
        return self._delete("/Customers/" + customer_id + "/PaymentMethod/" + payment_method_id)


    def update_payment_method(self, req):
        if 'customer_id' in req:
            customer_id = str(req['customer_id'])
        else:
            return self._error("customer_id is required")
        if 'payment_method_id' in req:
            payment_method_id = str(req['payment_method_id'])
        else:
            return self._error("payment_method_id is required")
        return self._put("/Customers/" + customer_id + '/PaymentMethod/' + payment_method_id, req)


    ################################################################################
    # Installment Plans
    ################################################################################

    def create_installment_plan(self, req):
        if 'customer_id' in req:
            customer_id = str(req['customer_id'])
        else:
            return self._error("customer_id is required")
        return self._post("/Customers/" + customer_id + "/PaymentSchedules/Installment", req)


    def create_variable_plan(self, req):
        if 'customer_id' in req:
            customer_id = str(req['customer_id'])
        else:
            return self._error("customer_id is required")
        return self._post("/Customers/" + customer_id + "/PaymentSchedules/Variable", req)


    def create_recurring_plan(self, req):
        if 'customer_id' in req:
            customer_id = str(req['customer_id'])
        else:
            return self._error("customer_id is required")
        return self._post("/Customers/" + customer_id + "/PaymentSchedules/Recurring", req)


    def get_plan(self, req):
        if 'customer_id' in req:
            customer_id = str(req['customer_id'])
        else:
            return self._error("customer_id is required")
        if 'plan_id' in req:
            plan_id = str(req['plan_id'])
        else:
            return self._error("plan_id is required")
        return self._get("/Customers/" + customer_id + "/PaymentSchedules/" + plan_id)


    def delete_plan(self, req):
        if 'customer_id' in req:
            customer_id = str(req['customer_id'])
        else:
            return self._error("customer_id is required")
        if 'plan_id' in req:
            plan_id = str(req['plan_id'])
        else:
            return self._error("plan_id is required")
        return self._delete("/Customers/" + customer_id + "/PaymentSchedules/" + plan_id)


    def update_recurring_plan(self, req):
        if 'customer_id' in req:
            customer_id = str(req['customer_id'])
        else:
            return self._error("customer_id is required")
        if 'plan_id' in req:
            plan_id = str(req['plan_id'])
        else:
            return self._error("plan_id is required")
        return self._put("/Customers/" + customer_id + "/PaymentSchedules/Recurring/" + plan_id, req)


    ################################################################################
    # Credits
    ################################################################################

    def credit(self, req):
        return self._post("/Payments/Credit", req)


    ################################################################################
    # Refunds and Voids
    ################################################################################

    def refund(self, req):
        if isinstance(req, dict):
            if 'transaction_id' in req:
                obj = dict
            else:
                return self._error("transaction_id is required")
        else:
            obj = {"transaction_id": req}
        return self._post("/Payments/Refund", obj)


    def void(self, req):
        if isinstance(req, dict):
            if 'transaction_id' in req:
                obj = dict
            else:
                return self._error("transaction_id is required")
        else:
            obj = {"transaction_id": req}
        return self._post("/Payments/Void", obj)


    ################################################################################
    # Transactions
    ################################################################################

    def get_transactions(self, req):
        if isinstance(req, dict):
            return self._post("/Transactions/Search", req)
        return self._get("/Transactions/" + str(req))


    def get_transaction(self, req):
        if isinstance(req, dict):
            if 'transaction_id' in req:
                transaction_id = str(req['transaction_id'])
            else:
                return self._error("transaction_id is required")
        else:
            transaction_id = str(req)
        return self._get("/Transactions/" + transaction_id)


    def update_transaction(self, req):
        if 'reference_transaction_id' in req:
            reference_transaction_id = str(req['reference_transaction_id'])
        else:
            return self._error("reference_transaction_id is required")
        return self._put("/Transactions/" + reference_transaction_id, req)
