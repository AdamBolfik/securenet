from distutils.core import setup

setup(
        name="securenet3",
        version="2.3",
        description="SecureNet Python3 Library",
        author="SecureNet",
        author_email="richard@richardstanford.com",
        url="https://gitlab.com/geothinQ/securenet",
        packages=["securenet"],
        install_requires=["requests >= 2.0"]
)
